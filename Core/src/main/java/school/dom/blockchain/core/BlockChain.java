package school.dom.blockchain.core;

import java.util.LinkedList;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public class BlockChain {
    public BlockChain() {
        blockStorage = new LinkedList<>();
        blockStorage.add(Block.GENESIS_BLOCK);
    }

    public Block getBlock(BlockHash hash) {
        return blockStorage.stream()
                .filter(block -> block.calculateHash().equals(hash))
                .findAny()
                .orElse(null);
    }

    public int size() {
        return blockStorage.size();
    }

    public Optional<Block> at(int i) {
        final int size = blockStorage.size();
        if(i >= 0 && i < size)
            return Optional.of(blockStorage.get(i));
        else
            return Optional.empty();
    }

    public Block getLastBlock() {
        return blockStorage.getLast();
    }

    public BlockHash getLastHash() {
        return getLastBlock().calculateHash();
    }

    public int getBitThreshold() { return zeroBitsThreshold; }

    public void setZeroBitsThreshold(int threshold) {
        this.zeroBitsThreshold = threshold;
    }

    public boolean addBlock(Block toAdd) {
        if (blockInsertionPolicy.apply(toAdd)) {
            blockStorage.add(toAdd);
            return true;
        } else {
            return false;
        }
    }

    public Optional<Integer> findFirstInvalid() {
        for (int i = 0; i < blockStorage.size() - 1; ++i) {
            Block prev = blockStorage.get(i);
            Block next = blockStorage.get(i+1);
            if (!prev.calculateHash().equals(next.getPreviousHash()))
                return Optional.of(i+1);
        }

        return Optional.empty();
    }

    public boolean isValid() {
        for (int i = 0; i < blockStorage.size() - 1; ++i) {
            Block prev = blockStorage.get(i);
            Block next = blockStorage.get(i+1);
            if (!prev.calculateHash().equals(next.getPreviousHash()))
                return false;
        }

        return true;
    }

    public Stream<Block> accessBlocksAsStream() {
        return blockStorage.stream();
    }

    private final Function<Block, Boolean> blockInsertionPolicy = new Function<>() {
        @Override
        public Boolean apply(Block toValidate) {
            BlockHash hash = toValidate.calculateHash();
            return toValidate.getPreviousHash().equals(getLastHash())
                    && hash.asBigInteger().getLowestSetBit() >= zeroBitsThreshold;
        }
    };

    private int zeroBitsThreshold = 12;
    private final LinkedList<Block> blockStorage;
}
