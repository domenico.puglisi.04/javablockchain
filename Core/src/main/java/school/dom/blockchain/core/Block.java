package school.dom.blockchain.core;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;

public class Block {
    public BlockData getData() {
        return data;
    }

    // If BlockData were marked as final, this method would NOT compile
    public void setData(BlockData data) {
        this.data = data;
    }

    public String getDescription() {
        return data.getDescription();
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public BlockHash getPreviousHash() {
        return previousHash;
    }

    public int getNonce() {
        return nonce;
    }

    public void incrementNonce() {
        nonce++;
    }

    public BlockHash calculateHash() {
        ByteBuffer b = ByteBuffer.allocate(data.getSize() + previousHash.value().length + Long.BYTES + Integer.BYTES);
        b.put(data.getData());
        b.put(previousHash.value());
        b.putLong(timestamp.getEpochSecond());
        b.putInt(nonce);

        return new BlockHash(HASH_GENERATOR.digest(b.array()));
    }

    // BlockData should by all means be final, but for the purposes of this program,
    // it's left mutable to allow block validity computation to return false to prove the blockchain's integrity.
    private /* final */ BlockData data;
    private final Instant timestamp;
    private final BlockHash previousHash;
    private int nonce = 0;

    public static final int MAX_BLOCK_DATA_SIZE = 1024;
    public static final int HASH_LENGTH;
    public static final MessageDigest HASH_GENERATOR;
    public static final Block GENESIS_BLOCK = new Block();

    static {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.exit(-1);
            // the fact that this can throw is very dumb
        } finally {
            HASH_GENERATOR = md;
            HASH_LENGTH = md.getDigestLength();
        }
    }

     public Block(BlockData data, BlockHash previousHash) {
        this.data = data;
        this.timestamp = Instant.now();
        this.previousHash = previousHash;
    }

    // To be used ONLY to generate the first block or a copy of it
    private Block() {
        this.data = new GenesisBlockData();
        this.timestamp = Instant.EPOCH;
        this.previousHash = new BlockHash(new byte[HASH_LENGTH]);
    }

    private static class GenesisBlockData implements BlockData {
        @Override
        public String getDescription() {
            return "GENESIS BLOCK";
        }

        @Override
        public byte[] getData() {
            return data;
        }

        @Override
        public int getSize() {
            return 0;
        }

        @Override
        public void setData(byte[] data) {}

        @Override
        public String displayData() {
            return "[[GENESIS BLOCK]]";
        }

        private final byte[] data = new byte[0];
    }
}