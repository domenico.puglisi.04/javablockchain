package school.dom.blockchain.core;

import java.nio.charset.StandardCharsets;

public class BlockTextData implements BlockData {
    public BlockTextData(String data) {
        this.data = data;
    }

    @Override
    public String getDescription() {
        return "UTF-8 STRING";
    }

    @Override
    public byte[] getData() {
        return data.getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public int getSize() {
        return data.getBytes(StandardCharsets.UTF_8).length;
    }

    @Override
    public void setData(byte[] data) {
        this.data = new String(data, StandardCharsets.UTF_8);
    }

    @Override
    public String displayData() {
        return data;
    }

    private String data;
}
