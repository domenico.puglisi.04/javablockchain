package school.dom.blockchain.core;

import java.math.BigInteger;
import java.util.Arrays;

public record BlockHash(byte[] value) {
    public String asHexString() {
        StringBuilder sb = new StringBuilder();
        for (byte b : value) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public BigInteger asBigInteger() {
        return new BigInteger(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlockHash blockHash = (BlockHash) o;
        return Arrays.equals(value, blockHash.value);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(value);
    }
}
