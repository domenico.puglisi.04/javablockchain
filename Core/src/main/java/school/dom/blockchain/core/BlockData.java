package school.dom.blockchain.core;

public interface BlockData {
    String getDescription();
    byte[] getData();
    int    getSize();

    void   setData(byte[] data);
    String displayData();
}
