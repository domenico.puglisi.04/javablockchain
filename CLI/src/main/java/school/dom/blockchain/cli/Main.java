package school.dom.blockchain.cli;

import school.dom.blockchain.core.Block;
import school.dom.blockchain.core.BlockChain;
import school.dom.blockchain.core.BlockData;
import school.dom.blockchain.core.BlockTextData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        final BufferedReader kbd = new BufferedReader(new InputStreamReader(System.in));
        final BlockChain bc = new BlockChain();

        String[] lastInput = { "add", "Hello World!" };

        boolean shouldExit = false;
        while (!shouldExit) {
            System.out.print("> ");
            String[] input;
            try {
                input = kbd.readLine().split(" ", 2);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            if (input[0].isBlank())
                input = lastInput;

            switch (input[0].toLowerCase()) {
                case "get" -> System.out.println(
                    "Current rightmost zero bits required to add a block: " + bc.getBitThreshold()
                );
                case "set" -> parseInteger(input[1]).ifPresent(
                        bc::setZeroBitsThreshold
                );
                case "inc", "increment" -> {
                    final int newThreshold = (bc.getBitThreshold() + 1) % 32;
                    bc.setZeroBitsThreshold(newThreshold);
                }
                case "dec", "decrement" -> {
                    final int newThreshold;
                    if (bc.getBitThreshold() - 1 < 0)
                        newThreshold = 32;
                    else
                        newThreshold = bc.getBitThreshold() - 1;
                    bc.setZeroBitsThreshold(newThreshold);
                }
                case "add" -> {
                    final BlockData data = new BlockTextData(input[1]);

                    // Time insertion
                    final long t0 = System.currentTimeMillis();
                    Block b = mineUntilBlockAdd(bc, data);
                    final long t1 = System.currentTimeMillis();

                    System.out.printf(
                            "Block added; nonce = %d, took %dms to mine.\n",
                            b.getNonce(),
                            t1 - t0
                    );
                }
                case "edit" -> {
                    String[] cmd = input[1].split(" ", 2);
                    if (cmd.length == 2) {
                        BlockTextData data = new BlockTextData(cmd[1]);
                        parseInteger(cmd[0]).ifPresent(
                               index -> {
                                   if (index < 0)
                                       index = bc.size() + index;
                                   bc.at(index).ifPresent(block -> block.setData(data));
                               }
                        );
                    }
                }
                case "print" -> {
                    Optional<Integer> index = Optional.empty();
                    if (input.length == 2)
                        index = parseInteger(input[1]);

                    index.ifPresentOrElse(
                            blockPosition -> {
                                if (blockPosition < 0)
                                    blockPosition = bc.size() + blockPosition;
                                for (int i = blockPosition - 1; i <= blockPosition + 1; ++i)
                                    bc.at(i).ifPresent(Main::printBlockData);
                            },
                            () -> bc.accessBlocksAsStream().forEach(Main::printBlockData)
                    );
                }
                case "validate" ->
                    bc.findFirstInvalid().ifPresentOrElse(
                            n  -> System.out.println("Blockchain is invalid; first invalid block is: " + n),
                            () -> System.out.println("Blockchain is valid.")
                    );
                case "quit", "exit" -> shouldExit = true;
                case "help", "?" -> System.out.print(
                            """
                            get                := Displays the current zero bit threshold for valid block hashes.
                            set <num>          := Updates the current zero bit threshold to <num>.
                            inc, increment     := Increments the current zero bit threshold by one (max: 32).
                            dec, decrement     := Decrements the current zero bit threshold by one (min: 0).
                            add <content>      := Creates new block with specified contents and mines until insertion.
                            edit <n> <content> := Updates block <n> with new content.
                                                  Negative indexes allow reversed access to the collection.
                            print [n]          := Prints the content of the blockchain. If <n> is specified,
                                                  displays only the n-th block and its neighbours.
                            validate           := Checks for the validity of block hashes in the chain.
                            quit, exit         := Exits the program.
                            help, ?            := Displays this help text.
                            """
                );
            }

            // Update history
            lastInput = input;
        }
    }

    public static Optional<Integer> parseInteger(String s) {
        int retval;
        try {
            retval = Integer.parseInt(s);
            return Optional.of(retval);
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

    private static Block mineUntilBlockAdd(BlockChain bc, BlockData data) {
        Block b = new Block(data, bc.getLastHash());
        while (true) {
            if (bc.addBlock(b))
                break;
            else
                b.incrementNonce();
        }

        return b;
    }

    private static void printBlockData(Block block) {
        System.out.printf(
                """
                BLOCK INFORMATION:
                DATA: %s
                DESCRIPTION: %s
                TIMESTAMP: %s (%d)
                NONCE: %d
                PREVIOUS HASH: %s
                BLOCK HASH:    %s
                
                """,
                block.getData().displayData(),
                block.getDescription(),
                LocalDateTime.ofInstant(block.getTimestamp(), ZoneId.systemDefault()),
                block.getTimestamp().getEpochSecond(),
                block.getNonce(),
                block.getPreviousHash().asHexString(),
                block.calculateHash().asHexString()
        );
    }
}
